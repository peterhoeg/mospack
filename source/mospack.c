/*
  MosPack
  version 0.92, 2005-03-26
  by Yacomo

  based on
  TisPack, a tileset compression utility
  version 0.91, 2004-05-14
  Copyright (C) 2004 Per Olofsson

  Notice from original tispack:

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the author be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Per Olofsson (MagerValp@cling.gu.se)

*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#ifdef DOS
#include <fcntl.h>
#endif
#include "version.h"
#include "mospack.h"
#include "jpegcompress.h"


char progname[] = "mospack";

#define DESTBUFSIZE 8192
#ifndef MAXFNAMELEN
#define MAXFNAMELEN 1024
#endif

/* arg defaults */
int silent = 0;
int quality = 75;
int blur = 0;
int keepbrokenmoz = 0;
int outputname = 0;
int haltonerror = 1;


/* Compress a tile using JPEG only */
int til2compress(unsigned char *src, unsigned char *dest, unsigned char *palettes, short width, short height) {
  unsigned char *bmptr = src;
  unsigned char rgbbuf[64*64*3];
  unsigned char *rgbptr = rgbbuf;
  unsigned char r[256];
  unsigned char g[256];
  unsigned char b[256];
  int x, y;
  int lenimg;

  /* extract palette */
  for (x = 0; x <= 255; ++x) {
    r[x] = palettes[x*4];
    g[x] = palettes[x*4+1];
    b[x] = palettes[x*4+2];
  }

  /* convert to rgb */
  for (y = 0; y < height; ++y) {
    for (x = 0; x < width; ++x) {
	*rgbptr++ = r[*bmptr];
	*rgbptr++ = g[*bmptr];
	*rgbptr++ = b[*bmptr++];
    }
  }

  strcpy(dest, "TIL2");

  if ((lenimg = jpegcompress(rgbbuf, dest + 6, width, height)) <= 0) {
    return(-1);
  } else {
    dest[4] = lenimg >> 8;
    dest[5] = lenimg & 0xff;
  }
  //printf("%d bytes jpeg compressed\n", lenimg);

  return(lenimg + 6);
}


int writetile(int outfile, unsigned char *buf, int buflen) {
  if (write(outfile, buf, buflen) != buflen) {
    printf("Write failed!\n");
    return(-1);
  } else {
    return(buflen);
  }
}


int mospack(int infile, int outfile, short width, short height, short columns, short rows) {
  int tile = 0, tilesize;
  short tilewidth, tileheight;
  int len;
  unsigned char *buffer;
  unsigned char tilebuf[DESTBUFSIZE];
  int compressedtilesize;
  int packedsum = 8;		/* header is 8 bytes */
  int filesize = 0;

  /* read palettes first */
  int palettesize = columns * rows * 1024;
  unsigned char *palettes;
  palettes = malloc(palettesize);
  if (palettes == NULL) {
    printf("Out of memory - cannot allocate memory for palettes.");
    return 1;
  }
  len = read(infile, palettes, palettesize);
  if (len != palettesize) {
    printf("Unexpected end of file - couldn't read palettes!");
    return 1;
  }
  if (silent < 1) {
    printf("Read %d bytes of palette information\n", len);
  }

  /* skip tile offsets */
  int tileoffsetsize = columns * rows * 4;
  unsigned char *tileoffsets;
  tileoffsets = malloc(tileoffsetsize);
  if (tileoffsets == NULL) {
    printf("Out of memory - cannot allocate memory for tile offsets.");
    return 1;
  }
  len = read(infile, tileoffsets, tileoffsetsize);
  if (len != tileoffsetsize) {
    printf("Unexpected end of file - couldn't read tile offsets!");
    return 1;
  }

  /* process tiles */
  int x,y;
  for (x = 0; x < rows; x++) {
    tileheight = ((x == rows - 1) && (height % 64 != 0)) ? height % 64 : 64;
    for (y = 0; y < columns; y++) {
      tilewidth = ((y == columns - 1) && (width % 64 != 0)) ? width % 64 : 64;
      tilesize = tilewidth * tileheight;
      buffer = malloc(tilesize);
      if (buffer == NULL) {
        printf("Out of memory - cannot allocate memory for tile %d.", tile);
        return 1;
      }

      len = read(infile, buffer, tilesize);
      if (len != tilesize) {
        printf("Unexpected end of file - couldn't read tile %d\n", tile);
        return 1;
      }

      if (silent < 1) {
        printf("Tile %d (%d x %d pixels): ", tile, tilewidth, tileheight);
      }

      /* compress the tile */
      if ((compressedtilesize = til2compress(buffer, tilebuf, palettes + tile * 1024, tilewidth, tileheight)) <= 0) {
        return(1);
      }

      if (silent < 1) {
        printf("%d -> %d bytes (%.1f%c)\n", tilesize, compressedtilesize, 100.0*compressedtilesize / tilesize + 0.5, '%');
      }

      /* write the tile */
      if (writetile(outfile, tilebuf, compressedtilesize) < 0) {
        printf("Writing of tile %d failed!\n", tile);
        return(1);
      }

      filesize += tilesize;
      packedsum += compressedtilesize;

      /* next tile */
      ++tile;
    }
  }

  if (silent < 2) {
    printf("packed %d tiles\n", tile);
    if (filesize) {
      printf("output size: %d bytes (%.1f%c)\n", packedsum, 100.0*packedsum/filesize + 0.5, '%');
    }
  }
  return(0);
}


int writemozheader(int outfile, short width, short height) {
  unsigned char h[8];

  h[0] = 'M';
  h[1] = 'O';
  h[2] = 'Z';
  h[3] = '0';
  h[4] = width >>8;
  h[5] = width & 0xff;
  h[6] = height >>8;
  h[7] = height & 0xff;

  if (write(outfile, h, 8) != 8) {
    printf("MOZ header write failed!\n");
    return(-1);
  } else {
    return(8);
  }
}


int convertmos(char *infname, char *outfname) {
  int infile, outfile, status, numtiles;
  struct stat s;
  unsigned char h[24];
  short width, height, columns, rows;
  int blocksize, paletteoffset;

  if (stat(infname, &s) != 0) {
    printf("Couldn't find %s\n", infname);
    return(1);
  }

  if ((infile = open(infname, O_RDONLY)) == -1) {
    printf("Couldn't open %s\n", infname);
    return(1);
  }
#ifdef DOS
  /* dos sucks */
  setmode(infile, O_BINARY);
#endif

  if (s.st_size == 0) {
    printf("%s is empty\n", infname);
  }

  if (read(infile, &h, 24) != 24) {
    printf("Couldn't read MOS header\n");
    return(-1);
  }
  if (strncmp(h, "MOS V1  ", 8) != 0) {
    return(-1);
  }

  width = h[8] | h[9]<<8;
  height = h[10] | h[11]<<8;
  columns = h[12] | h[13]<<8;
  rows = h[14] | h[15]<<8;
  blocksize = h[16] | h[17]<<8 | h[18]<<16 | h[19]<<24;
  paletteoffset = h[20] | h[21]<<8 | h[22]<<16 | h[23]<<24;

  if (blocksize != 64 || paletteoffset != 24) {
    printf("%s doesn't look like a MOS file\n", infname);
    close(infile);
    return(1);
  }

  numtiles = columns * rows;

  if (silent < 2) {
    printf("Packing mos of size %d x %d (%d tiles) in %s to %s\n", width, height, numtiles, infname, outfname);
  }

  if (stat(outfname, &s) == 0) {
    printf("Overwriting %s\n", outfname);
    unlink(outfname);
  }

  if ((outfile = open(outfname, O_WRONLY | O_CREAT, 0777)) == -1) {
    printf("Couldn't open %s for writing\n", outfname);
    close(infile);
    return(1);
  }
#ifdef DOS
  /* dos sucks */
  setmode(outfile, O_BINARY);
#endif

  if (writemozheader(outfile, width, height) < 0) {
    close(outfile);
    close(infile);
    return(1);
  }

  status = mospack(infile, outfile, width, height, columns, rows);
  close(outfile);
  close(infile);
  if (status) {
    if (!keepbrokenmoz) {
      unlink(outfname);
    }
  }

  return(status);
}


void usage() {
  printf("\nUsage: %s [-s] [-q quality] [-o outfile.moz] [-e] [-V] infile.mos\n", progname);
  puts("\nargs:");
  puts("  -s              Be silent. Supply twice for really silent.");
  puts("  -q quality      Set the compression quality, between 1-100.");
  puts("                  The default is 75.");
  puts("  -b blur         Set the amount of blur used with lossy compression,");
  puts("                  between 1-100. The default is no blur.");
  puts("  -o outfile.moz  Select output file. The default is <infile>.moz.");
  puts("  -e              Do not halt on errors.");
  puts("  -V              Print version number and exit\n");
}


/* change .mos extension to .moz */
void mos2moz(unsigned char *fname, int max) {
  unsigned char *c;

  /* find the extension */
  if ((c = strrchr(fname, '.')) != NULL) {
    /* check if it's .mos */
    if ((c[1] == 'm' || c[1] == 'M') &&
	(c[2] == 'o' || c[2] == 'O') &&
	(c[3] == 's' || c[3] == 'S') &&
	(c[4] == 0)) {
      /* then change it to .moz */
      c[3] += 7;
      return;
    }
  }

  /* else just add .moz if it fits */
  if (strlen(fname) < max - 5) {
    strcat(fname, ".moz");
  } else {
    /* overwrite the end of the filename if .moz doesn't fit */
    fname[max-5] = '.';
    fname[max-4] = 'm';
    fname[max-3] = 'o';
    fname[max-2] = 'z';
    fname[max-1] = 0;
  }
}


int main(int argc, char *argv[]) {
  char *infname = NULL;
  int i;
  char outfname[MAXFNAMELEN];

  if (argc == 1) {
    usage();
    return(1);
  } else {

    /* loop through the args */
    i = 1;
    while (i < argc) {

      if (argv[i][0] != '-') {
	break;
      }

      if (strlen(argv[i]) != 2) {
	printf("unrecognized arg \"%s\"\n", argv[i]);
	usage();
	return(1);
      }

      /* eval args */
      switch (argv[i][1]) {

      case 's':
	silent += 1;
	break;

      case 'k':
	keepbrokenmoz = 1;
	break;

      case 'q':
	if (i < argc - 1) {
	  quality = atoi(argv[i+1]);
	  if (quality < 1 || quality > 100) {
	    printf("Illegal quality: %s", argv[i+1]);
	    usage();
	    return(1);
	  }
	  ++i;			/* skip next arg */
	} else {
	  usage();
	  return(1);
	}
	break;

      case 'b':
	if (i < argc - 1) {
	  blur = atoi(argv[i+1]);
	  if (blur < 1 || blur > 100) {
	    printf("Illegal blur amount: %s%c", argv[i+1], '%');
	    usage();
	    return(1);
	  }
	  ++i;			/* skip next arg */
	} else {
	  usage();
	  return(1);
	}
	break;

      case 'o':
	outputname = 1;
	if (i < argc - 1) {
	  strncpy(outfname, argv[i+1], MAXFNAMELEN - 1);
	  outfname[MAXFNAMELEN - 1] = 0;
	  ++i;			/* skip next arg */
	} else {
	  usage();
	  return(1);
	}
	break;

      case 'e':
	haltonerror = 0;
	break;

      case 'V':
	printf("%s %d.%d %s\n", progname, vers_major, vers_minor, copyright);
	return(0);
	break;

      default:
	printf("unrecognized arg \"%s\"\n", argv[i]);
	usage();
	return(1);

      }

      ++i;
    }
  }

  if (i == argc && infname == NULL) {
    puts("No input files");
    usage();
    return(1);
  }

  if (outputname) {
    if (i != argc - 1) {
      printf("Cannot specify -o with multiple source files\n");
      usage();
      return(1);
    }
    infname = argv[i];
    if (infname[0] == '-') {
      usage();
      return(1);
    }
    if (silent < 2) printf("Processing %s...\n", infname);
    return(convertmos(infname, outfname));
  } else {
    while (i < argc) {
      infname = argv[i];
      strncpy(outfname, infname, MAXFNAMELEN - 1);
      outfname[MAXFNAMELEN - 1] = 0;
      mos2moz(outfname, MAXFNAMELEN);
      if (silent < 2) printf("Processing %s...\n", infname);
      if (convertmos(infname, outfname) && haltonerror) {
	return(1);
      }
      ++i;
    }
  }

  return(0);
}
