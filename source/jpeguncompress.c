#include <stdio.h>
#include <stdlib.h>
#include <jpeglib.h>
#include <setjmp.h>
#include "mospack.h"


static unsigned char *srcbuf;
static int srcsize;
static struct jpeg_source_mgr jpg_src;

void jpg_init_src(j_decompress_ptr cinfo) {
  //printf("jpg_init_src called\n");
  cinfo->src->next_input_byte = srcbuf;
  cinfo->src->bytes_in_buffer = srcsize;
  //printf("srcbuffer is now %d bytes\n", srcsize);
}


static unsigned char eoibuf[] = {
  0xff, JPEG_EOI
};


boolean jpg_fill_srcbuf(j_decompress_ptr cinfo) {
  printf("jpg_fill_srcbuf called, jpeg decoding failed!\n");
  cinfo->src->next_input_byte = eoibuf;
  cinfo->src->bytes_in_buffer = 2;
  return(TRUE);
}


void jpg_skip_data(j_decompress_ptr cinfo, long num_bytes) {
  if (num_bytes > 0) {
    cinfo->src->next_input_byte += num_bytes;
    cinfo->src->bytes_in_buffer -= num_bytes;
  }
}


void jpg_term_src(j_decompress_ptr cinfo) {
  //printf("jpg_term_src\n");
}


void jpg_membuf_src(j_decompress_ptr cinfo, unsigned char *src, int size) {
  //printf("jpg_membuf_src called\n");
  srcbuf = src;
  srcsize = size;

  jpg_src.init_source = &jpg_init_src;
  jpg_src.fill_input_buffer = &jpg_fill_srcbuf;
  jpg_src.skip_input_data = &jpg_skip_data;
  jpg_src.resync_to_restart = &jpeg_resync_to_restart; /* default */
  jpg_src.term_source = &jpg_term_src;
  cinfo->src = &jpg_src;
}


/* Error handling � la IJG */
struct my_error_mgr {
  struct jpeg_error_mgr pub;	/* "public" fields */

  jmp_buf setjmp_buffer;	/* for return to caller */
};

typedef struct my_error_mgr * my_error_ptr;

METHODDEF(void)
my_error_exit (j_common_ptr cinfo)
{
  my_error_ptr myerr = (my_error_ptr) cinfo->err;
  (*cinfo->err->output_message) (cinfo);
  longjmp(myerr->setjmp_buffer, 1);
}


/* Uncompress a 64x64 pixel bitmap using JPEG compression. Saves palette
   at dest + 0 in {R,G,B,a} format, and 8-bit bitmap at dest + 1024 */
int jpeguncompress(unsigned char *src, int size, unsigned char *dest, int dither, JSAMPARRAY pal, short width, short height, unsigned char *palettes) {
  struct jpeg_decompress_struct cinfo;
  struct my_error_mgr jerr;
  JSAMPARRAY buffer;
  int len = 0;
  JSAMPLE *jpgpal[3];
  int i;

  //printf("Initializing JPEG uncompression.\n");
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = my_error_exit;
  if (setjmp(jerr.setjmp_buffer)) {
    jpeg_destroy_decompress(&cinfo);
    return(-1);
  }

  jpeg_create_decompress(&cinfo);
  jpg_membuf_src(&cinfo, src, size);
  (void) jpeg_read_header(&cinfo, TRUE);

  //printf("Setting 8-bit mode.\n");
  cinfo.quantize_colors = TRUE;
  if (dither) {
    cinfo.dither_mode = JDITHER_NONE;
  } else {
    cinfo.dither_mode = JDITHER_FS;
  }
  if (pal) {
    //printf("Uncompressing JPEG with predefined palette.\n");
    jpgpal[0] = pal[0] + 1;
    jpgpal[1] = pal[1] + 1;
    jpgpal[2] = pal[2] + 1;
    cinfo.actual_number_of_colors = 255;
    cinfo.colormap = (JSAMPARRAY) jpgpal;
  } else {
    //printf("Uncompressing JPEG without palette.\n");
    cinfo.desired_number_of_colors = 256;
    cinfo.two_pass_quantize = TRUE;
    cinfo.colormap = NULL;
  }

  //printf("Starting uncompression.\n");
  (void) jpeg_start_decompress(&cinfo);
  buffer = (*cinfo.mem->alloc_sarray)
		((j_common_ptr) &cinfo, JPOOL_IMAGE, 4096, 1);
  while (cinfo.output_scanline < cinfo.output_height) {
    //printf("Line %d\n", cinfo.output_scanline);
    (void) jpeg_read_scanlines(&cinfo, buffer, 1);
    memcpy(dest + len, buffer[0], width);
    len += width;
  }

  if (pal) {
    //printf("Storing predefined palette.\n");
    for (i = 0; i < 256; ++i) {
      palettes[i*4]   = pal[0][i];
      palettes[i*4+1] = pal[1][i];
      palettes[i*4+2] = pal[2][i];
      palettes[i*4+3] = 0;
    }
    //printf("Adjusting for alpha.\n");
    /* adjust palette */
    palettes += 1024;
    for (i = 0; i < 64*64; ++i) {
      *palettes++ += 1;
    }
  } else {
    //printf("Checking colormap.\n");
    if (cinfo.colormap) {
      //printf("Found colormap.\n");
      for (i = 0; i <= 2; ++i) {
	//printf("%d\n", i);
	if (cinfo.colormap[i] == NULL) {
	  //printf("there's no colormap for component %d!\n", i);
	}
      }
      //printf("Storing generated palette.\n");
      for (i = 0; i < 256; ++i) {
	//printf("%4d", i);
	palettes[i*4]   = cinfo.colormap[0][i];
	palettes[i*4+1] = cinfo.colormap[1][i];
	palettes[i*4+2] = cinfo.colormap[2][i];
	palettes[i*4+3] = 0;
      }
      //printf("\n");
    } else {
      //printf("There's no generated palette!\n");
      return(-1);
    }
  }

  //printf("Finishing.\n");
  (void) jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);


  return(len);
}
