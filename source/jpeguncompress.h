/* Uncompress a 64x64 pixel bitmap using JPEG compression */
int jpeguncompress(unsigned char *src, int size, unsigned char *dest, int dither, JSAMPARRAY pal, short width, short height, unsigned char *palettes);
