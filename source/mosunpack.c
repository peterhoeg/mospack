/*
  MosPack
  version 0.92, 2005-03-26
  by Yacomo

  based on
  TisPack, a tileset compression utility
  version 0.91, 2004-05-14
  Copyright (C) 2004 Per Olofsson

  Notice from original tispack:

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the author be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Per Olofsson (MagerValp@cling.gu.se)

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <jpeglib.h>
#ifdef DOS
#include <fcntl.h>
#endif
#include "version.h"
#include "jpeguncompress.h"


char progname[] = "mosunpack";

#define BUFSIZE 8192
#ifndef MAXFNAMELEN
#define MAXFNAMELEN 1024
#endif

int silent = 0;
int force = 0;
int dither = 0;
int keepbrokenmos = 0;
int outputname = 0;
int haltonerror = 1;


int til2uncompress(unsigned char *src, unsigned char *dest, unsigned char *palettes, short tilelen, short width, short height) {
  if (jpeguncompress(src, tilelen, dest, dither, NULL, width, height, palettes) != width*height) {
    printf("TIL2 uncompress failed, jpeg corrupt\n");
    return(-1);
  }

  return(5120);
}


#define doread(FH, BUF, LEN) if ((l = read((FH), (BUF), (LEN))) != (LEN)) {printf("Read of %d bytes failed: %d (got %d)!\n", (LEN), errno, l); close((FH)); return(1);}


int mozunpack(int infile, int outfile, short width, short height, short columns, short rows) {
  int tile = 0, tilesize;
  short tilewidth, tileheight;
  int len;
  unsigned char buf[BUFSIZE];
  int unpackedsum = 24;		/* header is 24 bytes */
  int tilelen, l;

  /* allocate buffer for palettes */
  int palettesize = columns * rows * 1024;
  unsigned char *palettes;
  unsigned char *currentpalette;
  palettes = malloc(palettesize);
  if (palettes == NULL) {
    printf("Out of memory - cannot allocate memory for palettes.");
    return 1;
  }
  currentpalette = palettes;

  /* allocate buffer for tile offsets */
  int tileoffsetsize = columns * rows * 4;
  unsigned char *tileoffsetbuf;
  int currenttileoffset = 0;
  tileoffsetbuf = malloc(tileoffsetsize);
  if (tileoffsetbuf == NULL) {
    printf("Out of memory - cannot allocate memory for tile offsets.");
    return 1;
  }
  unsigned int tileoffsetvalue = 0;

  /* allocate buffer for image */
  int imagesize = width * height;
  unsigned char *imagebuf;
  unsigned char *currenttile;
  imagebuf = malloc(imagesize);
  if (imagebuf == NULL) {
    printf("Out of memory - cannot allocate memory for tile data.");
    return 1;
  }
  currenttile = imagebuf;

  int x,y;
  for (x = 0; x < rows; x++) {
    tileheight = ((x == rows - 1) && (height % 64 != 0)) ? height % 64 : 64;
    for (y = 0; y < columns; y++) {
      tilewidth = ((y == columns - 1) && (width % 64 != 0)) ? width % 64 : 64;
      tilesize = tilewidth * tileheight;
      tileoffsetbuf[currenttileoffset] = tileoffsetvalue & 0xff;
      tileoffsetbuf[currenttileoffset+1] = tileoffsetvalue >> 8 & 0xff;
      tileoffsetbuf[currenttileoffset+2] = tileoffsetvalue >> 16 & 0xff;
      tileoffsetbuf[currenttileoffset+3] = tileoffsetvalue >> 24 & 0xff;
      tileoffsetvalue += tilesize;

      len = read(infile, buf, 6);
      if (len != 6) {
        printf("Tile %d corrupted!", tile);
      }

      if (strncmp(buf, "TIL2", 4)) {
	  printf("Garbage at tile %d: 0x%02x 0x%02x 0x%02x 0x%02x\n", tile, buf[0], buf[1], buf[2], buf[3]);
	  return(1);
      }

      if (silent < 1) {
        printf("Tile %d (%d x %d pixels): ", tile, tilewidth, tileheight);
      }

      tilelen = (buf[4] << 8 | buf[5]);
      doread(infile, buf, tilelen);

      if (silent < 1) {
	printf("%d bytes\n", tilelen);
      }
      til2uncompress(buf, currenttile, currentpalette, tilelen, tilewidth, tileheight);
      currenttile += tilesize;
      currentpalette += 1024;
      currenttileoffset += 4;

      ++tile;
    }
  }

  if (write(outfile, palettes, palettesize) != palettesize) {
    printf("Write of palettes failed!\n");
    return(1);
  }

  if (write(outfile, tileoffsetbuf, tileoffsetsize) != tileoffsetsize) {
    printf("Write of tile offsets failed!\n");
    return(1);
  }

  if (write(outfile, imagebuf, imagesize) != imagesize) {
    printf("Write of image failed!\n");
    return(1);
  }

  if (silent < 2) {
    printf("unpacked %d tiles\n", tile);
    if (unpackedsum > 24) {
	printf("output size: %d bytes\n", unpackedsum);
    }
  }
  return(0);
}


static unsigned char mosheader[] = {
  'M', 'O', 'S', ' ',
  'V', '1', ' ', ' ',
  0, 0, 0, 0,
  0, 0, 0, 0,
  0x40, 0x00, 0x00, 0x00,
  0x18, 0x00, 0x00, 0x00
};


int writemosheader(int outfile, short width, short height, short columns, short rows) {
  unsigned char h[24];

  memcpy(h, mosheader, 24);
  h[8] = width & 0xff;
  h[9] = width >> 8;
  h[10] = height & 0xff;
  h[11] = height >> 8;
  h[12] = columns & 0xff;
  h[13] = columns >> 8;
  h[14] = rows & 0xff;
  h[15] = rows >> 8;

  if (write(outfile, h, 24) != 24) {
    printf("MOS header write failed!\n");
    return(-1);
  } else {
    return(24);
  }
}


int convertmoz(char *infname, char *outfname) {
  int infile, outfile, status, numtiles;
  struct stat s;
  short width, height, columns, rows;

  if (stat(outfname, &s) == 0) {
    if (force == 0) {
      printf("%s already exists, use -f to force overwrite.\n", outfname);
      return(1);
    } else {
      unlink(outfname);
    }
  }

  if ((infile = open(infname, O_RDONLY)) == -1) {
    printf("Couldn't open %s\n", infname);
    return(1);
  }
#ifdef DOS
  /* dos sucks */
  setmode(infile, O_BINARY);
#endif


  unsigned char h[8];

  if (read(infile, &h, 8) != 8) {
    printf("Couldn't read MOZ header\n");
    return(-1);
  }
  if (strncmp(h, "MOZ0", 4) != 0) {
    printf("Not a MOZ version 0 header\n");
  return(-1);
  }

  width = h[4]<<8 | h[5];
  height = h[6]<<8 | h[7];
  columns = width / 64 + (width % 64 != 0);
  rows = height / 64 + (height % 64 != 0);
  numtiles = columns * rows;

  if ((outfile = open(outfname, O_WRONLY | O_CREAT, 0777)) == -1) {
    printf("Couldn't open %s for writing\n", outfname);
    close(infile);
    return(1);
  }
#ifdef DOS
  /* dos sucks */
  setmode(outfile, O_BINARY);
#endif

  if (silent < 2) {
    printf("Unpacking mos of size %d x %d (%d tiles) in %s to %s\n", width, height, numtiles, infname, outfname);
  }

  if (writemosheader(outfile, width, height, columns, rows) < 0) {
    close(outfile);
    close(infile);
    return(1);
  }

  status = mozunpack(infile, outfile, width, height, columns, rows);
  close(outfile);
  close(infile);
  if (status) {
    if (!keepbrokenmos) {
      unlink(outfname);
    }
  }

  return(status);
}


void usage() {
  printf("\nUsage: %s [-s] [-f] [-o outfile.mos] infile.moz\n", progname);
  puts("\nargs:");
  //puts("  -p              Print ascii greyscale tile preview.");
  puts("  -s              Be silent. Supply twice for really silent.");
  puts("  -f              Force overwrite of existing output file.");
  puts("  -d              Dither 24-bit tiles.");
  puts("  -o outfile.mos  Select output file. The default is <infile>.mos.");
  puts("  -e              Do not halt on errors.");
  puts("  -V              Print version number and exit\n");
}


/* change .moz extension to .mos */
void moz2mos(unsigned char *fname, int max) {
  unsigned char *c;

  /* find the extension */
  if ((c = strrchr(fname, '.')) != NULL) {
    /* check if it's .moz */
    if ((c[1] == 'm' || c[1] == 'M') &&
	(c[2] == 'o' || c[2] == 'O') &&
	(c[3] == 'z' || c[3] == 'Z') &&
	(c[4] == 0)) {
      /* then change it to .mos */
      c[3] -= 7;
      return;
    }
  }

  /* else just add .mos if it fits */
  if (strlen(fname) < max - 5) {
    strcat(fname, ".mos");
  } else {
    /* overwrite the end of the filename if .mos doesn't fit */
    fname[max-5] = '.';
    fname[max-4] = 'm';
    fname[max-3] = 'o';
    fname[max-2] = 's';
    fname[max-1] = 0;
  }
}


int main(int argc, char *argv[]) {
  char *infname = NULL;
  int i;
  char outfname[MAXFNAMELEN];

  if (argc == 1) {
    usage();
    return(1);
  } else {

    /* loop through the args */
    i = 1;
    while (i < argc) {

      if (argv[i][0] != '-') {
	break;
      }

      if (strlen(argv[i]) != 2) {
	printf("unrecognized arg \"%s\"\n", argv[i]);
	usage();
	return(1);
      }

      /* eval args */
      switch (argv[i][1]) {

      case 's':
	silent += 1;
	break;

      case 'd':
	dither = 1;
	break;

      case 'f':
	force = 1;
	break;

      case 'k':
	keepbrokenmos = 1;
	break;

      case 'o':
	outputname = 1;
	if (i < argc - 1) {
	  strncpy(outfname, argv[i+1], MAXFNAMELEN - 1);
	  outfname[MAXFNAMELEN - 1] = 0;
	  ++i;			/* skip next arg */
	} else {
	  usage();
	  return(1);
	}
	break;

      case 'e':
	haltonerror = 0;
	break;

      case 'V':
	printf("%s %d.%d %s\n", progname, vers_major, vers_minor, copyright);
	return(0);
	break;

      default:
	printf("unrecognized arg \"%s\"\n", argv[i]);
	usage();
	return(1);

      }

      ++i;
    }
  }

  if (i == argc && infname == NULL) {
    puts("No input files");
    usage();
    return(1);
  }

  if (outputname) {
    if (i != argc - 1) {
      printf("Cannot specify -o with multiple source files\n");
      usage();
      return(1);
    }
    infname = argv[i];
    if (infname[0] == '-') {
      usage();
      return(1);
    }
    if (silent < 2) printf("Processing %s...\n", infname);
    return(convertmoz(infname, outfname));
  } else {
    while (i < argc) {
      infname = argv[i];
      strncpy(outfname, infname, MAXFNAMELEN - 1);
      outfname[MAXFNAMELEN - 1] = 0;
      moz2mos(outfname, MAXFNAMELEN);
      if (silent < 2) printf("Processing %s...\n", infname);
      if (convertmoz(infname, outfname) && haltonerror) {
	return(1);
      }
      ++i;
    }
  }

  return(0);
}
