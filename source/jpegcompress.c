#include <stdio.h>
#include <stdlib.h>
#include <jpeglib.h>
#include <string.h>
#include "mospack.h"


static unsigned char *destbuf;
static unsigned char *buf;
static int bufsize;
static struct jpeg_destination_mgr jpg_dest;

void jpg_init_dest(j_compress_ptr cinfo) {
  //printf("jpg_init_dest called\n");
  buf = malloc(DESTBUFSIZE*10);
  bufsize = 0;
  cinfo->dest->next_output_byte = buf;
  cinfo->dest->free_in_buffer = DESTBUFSIZE;
  //printf("destbuffer is now %d bytes\n", bufsize);
}


boolean jpg_empty_destbuf(j_compress_ptr cinfo) {
  int size;

  size = DESTBUFSIZE - cinfo->dest->free_in_buffer;
  //printf("jpg_empty_destbuf called, with %d bytes in the buffer\n", size);
  memcpy(destbuf + bufsize, buf, size);
  bufsize += size;
  //printf("destbuffer is now %d bytes\n", bufsize);
  cinfo->dest->next_output_byte = buf;
  cinfo->dest->free_in_buffer = DESTBUFSIZE;
  //printf("buffer copied and pointer reset\n");
  return(TRUE);
}


void jpg_term_dest(j_compress_ptr cinfo) {
  int size;

  size = DESTBUFSIZE - cinfo->dest->free_in_buffer;
  //printf("jpg_term_dest called, with %d bytes in the buffer\n", size);
  memcpy(destbuf + bufsize, buf, size);
  bufsize += size;
  //printf("destbuffer is now %d bytes\n", bufsize);
  free(buf);
}


void jpg_membuf_dest(j_compress_ptr cinfo, unsigned char *dest) {

  //printf("jpg_membuf_dest called\n");
  destbuf = dest;
  jpg_dest.init_destination = &jpg_init_dest;
  jpg_dest.empty_output_buffer = &jpg_empty_destbuf;
  jpg_dest.term_destination = &jpg_term_dest;
  cinfo->dest = &jpg_dest;
}


/* Compress a 24-bit 64x64 pixel bitmap in {R,G,B} format using JPEG
   with the given quality setting. */
int jpegcompress(unsigned char *src, unsigned char *dest, short width, short height) {
  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;
  JSAMPROW row_pointer[1];

  bufsize = 0;

  //printf("jpeg_std_error\n");
  cinfo.err = jpeg_std_error(&jerr);
  //printf("jpeg_create_compress\n");
  jpeg_create_compress(&cinfo);

  //printf("jpg_membuf_dest\n");
  jpg_membuf_dest(&cinfo, dest);

  //printf("setting parameters\n");
  cinfo.image_width = width; 	/* image width and height, in pixels */
  cinfo.image_height = height;
  cinfo.input_components = 3;		/* # of color components per pixel */
  cinfo.in_color_space = JCS_RGB; 	/* colorspace of input image */
  jpeg_set_defaults(&cinfo);
  jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);
  cinfo.smoothing_factor = blur;
  cinfo.optimize_coding = TRUE;

  //printf("jpeg_start_compress\n");
  jpeg_start_compress(&cinfo, TRUE);

  //printf("jpeg_write_scanlines\n");
  while (cinfo.next_scanline < cinfo.image_height) {
    row_pointer[0] = &src[cinfo.next_scanline * width*3];
    (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
  }

  //(void) jpeg_write_scanlines(&cinfo, (JSAMPARRAY) src, 64);

  //printf("jpeg_finish_compress\n");
  jpeg_finish_compress(&cinfo);

  //printf("jpeg_destroy_compress\n");
  jpeg_destroy_compress(&cinfo);

  return(bufsize);
}
