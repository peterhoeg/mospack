#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>


#define doread(FH, BUF, LEN) if ((l = read((FH), (BUF), (LEN))) != (LEN)) {printf("Read of %d bytes failed: %d (got %d)!\n", (LEN), errno, l); close((FH)); return(1);}


int main(int argc, char *argv[]) {
  int i, infile, numtiles, l;
  unsigned char buf[16384];
  short width, height, columns, rows;

  if (argc != 2) {
    return(1);
  }

  if ((infile = open(argv[1], O_RDONLY)) == -1) {
    printf("Couldn't open %s\n", argv[1]);
    return(1);
  }
#ifdef DOS
  /* dos sucks */
  setmode(infile, O_BINARY);
#endif


  doread(infile, &buf, 8);
  if (strncmp(buf, "MOZ0", 4)) {
    printf("Not a MOZ file\n");
    close(infile);
    return(1);
  }

  width = buf[4]<<8 | buf[5];
  height = buf[6]<<8 | buf[7];
  columns = width / 64 + (width % 64 != 0);
  rows = height / 64 + (height % 64 != 0);
  numtiles = columns * rows;
  printf("Found MOZ file version %c of size %d x %d (%d tiles)\n", buf[3], width, height, numtiles);

  for (i = 0; i < numtiles; ++i) {
    int tilelen, fh;
    unsigned char fname[16];

    doread(infile, &buf, 6);
    if (strncmp(buf, "TIL", 3)) {
      printf("Tile %d does not start with TIL\n", i);
      close(infile);
      return(1);
    }

    tilelen = (buf[4]<<8 | buf[5]);
    doread(infile, buf, tilelen);

    printf("Tile %d: %d bytes\n", i, tilelen);
    sprintf(fname, "tile%04d.jpg", i);
    if ((fh = open(fname, O_WRONLY | O_CREAT, 0777)) != -1) {
      printf("Saving %s", fname);
#ifdef DOS
      /* DOS sucks */
      setmode(fh, O_BINARY);
#endif
      if (write(fh, buf + 6, tilelen) != tilelen) {
	printf(" failed!\n");
      } else {
	printf("\n");
      }
      close(fh);
    }
  }

  return(0);
}
