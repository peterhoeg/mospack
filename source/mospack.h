extern unsigned char greyscale[];

#define DESTBUFSIZE 8192

/* arg defaults */
extern int printascii;
extern int silent;
extern int quality;
extern int blur;
